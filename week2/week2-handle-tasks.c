/*
 * HandleTasks (): call from main program
 */

void HandleTasks (void) {
	while (Pending) {
		int8_t i = NUMTASKS - 1; Pending = 0;
		do {
			// have to quit this loop when:
			// - all tasks have been performed e.g. i <= 0 or
			// - when a new task was submitted e.g. pending changed to 1
			Taskp t = &Tasks[i];
			if ((t->Activated != t->Invoked) && (t->Flags & TRIGGERED)) {
				// perform this task if its invocations are behind of the activations and
				// it is active (triggered)
				t->Taskf(); t->Invoked++;
			} else {
				// current task does not need to be performed right now, go to the next
				// (lower priority) task.
				i--;
			}
		} while (i >= 0 && !Pending);
	}
}