indices = [1 2 4 8 16 22 24 25 26 32];
edge_lists = cell(1, length(indices));
computation_time_lists = cell(1, length(indices));
computation_time_jitters = zeros(1, length(indices));
clock_period_lists = cell(1, length(indices));
clock_period_jitters = zeros(1, length(indices));

% loop through all data files
for n=1:length(indices)
	path = ['num_tasks_' num2str(indices(n)) '_trimmed.tim'];
	% read edge changes from a file containing lines like the following:
	%         Edge:               1254529.0 06
	%         Edge:               1295861.0 00
	edges = textread(path, 'Edge: %d .0 %*d');
	edge_lists{n} = edges;

	% Compute computation time: falling edge - rising edge
	computation_times = zeros(1, length(edges)/2);
	for i = 2:2:length(edges)
		computation_times(i/2) = edges(i) - edges(i - 1);
	end
	computation_time_lists{n} = computation_times;
	% Computation time jitter is always 0, I wonder if this could be the case
	% in practice. Perfect simulation is perfect.
	computation_time_jitters(n) = mean(abs(computation_times - mean(computation_times)));

	% Compute clock speed: rising edge - previous rising edge
	clock_periods = zeros(1, length(edges)/2 - 1);
	for i = 3:2:length(edges)
		clock_periods((i - 1)/2) = edges(i) - edges(i - 2);
	end
	clock_period_lists{n} = clock_periods;
	% Clock period jitter becomes zero when the computation time becomes
	% larger than the clock period. Is this because of the pending interrupt?
	clock_period_jitters(n) = mean(abs(clock_periods - mean(clock_periods)));
end

figure
scatter(indices, cellfun(@mean, computation_time_lists), 'filled')
title('computation time means')
text(indices, cellfun(@mean, computation_time_lists), ...
	arrayfun(@num2str, indices, 'UniformOutput', false), ...
	'HorizontalAlignment', 'left', 'VerticalAlignment', 'top')

figure
scatter(indices, cellfun(@mean, clock_period_lists), 'filled')
title('clock period means')
text(indices, cellfun(@mean, clock_period_lists), ...
	arrayfun(@num2str, indices, 'UniformOutput', false), ...
	'HorizontalAlignment', 'left', 'VerticalAlignment', 'top')
