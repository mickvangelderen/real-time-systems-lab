/***********************
 * Function `HandleTasks'
 ***********************/
HandleTasks:
.LFB8:
.LM19:
	push	r11
.LCFI0:
	push	r10
.LCFI1:
	/* prologue ends here (frame size = 0) */
.L__FrameSize_HandleTasks=0x0
.L__FrameOffset_HandleTasks=0x4
.LM20:
	mov.b	&BusyPrio, r10
.LVL10:
.LM21:
	mov.b	#0, &Pending
.LM22:
	mov.b	#9, &BusyPrio
.LM23:
	jmp	.L15
.LVL11:
.L19:
.LBB6:
.LM24:
	mov.b	&BusyPrio, r15
	call	#Prio2Taskp
	mov	r15, r11
.LM25:
	jmp	.L16
.L18:
.LM26:
	call	10(r11)
.L16:
.LM27:
	bit	#64,8(r11)
	jeq	.L17
	mov.b	6(r11), r14
	mov.b	7(r11), r15
	mov.b	r15, r13
	add.b	#1, r13
	mov.b	r13, 7(r11)
	cmp.b	r15, r14
	jne	.L18
.L17:
.LM28:
	add.b	#llo(-1), &BusyPrio
.L15:
.LBE6:
.LM29:
	cmp.b	r10, &BusyPrio
	jne	.L19

	/* epilogue: frame size = 0 */
.LM30:
	pop	r10
.LVL12:
	pop	r11
.LVL13:
	ret
.LFE8:
.Lfe5:
	.size	HandleTasks,.Lfe5-HandleTasks
;; End of function