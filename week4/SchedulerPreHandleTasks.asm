void HandleTasks (void) {
	int8_t oldBP = BusyPrio; // Save BusyPrio = current task handling level

	Pending = 0;             // This instance will handle all new
	// pending tasks.
	BusyPrio = NUMTASKS - 1; // Start at highest priority
	while (BusyPrio != oldBP) {
		Taskp CurTask = CurrentTask ();
		while (CurTask->Activated != CurTask->Invoked) {
			CurTask->Invoked++;
			if (CurTask->Flags & TRIGGERED) {
				_EINT(); CurTask->Taskf(); _DINT();
			} else CurTask->Activated = CurTask->Invoked;
		}
		BusyPrio--;
	}
}

/***********************
 * Function `HandleTasks'
 ***********************/
HandleTasks:
.LFB8:
.LM19:
	push	r11
.LCFI0:
	push	r10
.LCFI1:
	/* prologue ends here (frame size = 0) */
.L__FrameSize_HandleTasks=0x0
.L__FrameOffset_HandleTasks=0x4
.LM20:
	mov.b	&BusyPrio, r10
.LVL10:
.LM21:
	mov.b	#0, &Pending
.LM22:
	mov.b	#9, &BusyPrio
.LM23:
	jmp	.L15
.LVL11:
.L19:
.LBB6:
.LM24:
	mov.b	&BusyPrio, r15
	call	#Prio2Taskp
	mov	r15, r11
.LM25:
	jmp	.L21
.L18:
.LM26:
	add.b	#1, r15
	mov.b	r15, 7(r11)
.LM27:
	bit	#64,8(r11)
	jeq	.L17
.LM28:
/* #APP */
 ;  157 "SchedulerPre.c" 1
	eint
 ;  0 "" 2
/* #NOAPP */
	call	10(r11)
/* #APP */
 ;  157 "SchedulerPre.c" 1
	dint
 ;  0 "" 2
/* #NOAPP */
	jmp	.L21
.L17:
.LM29:
	mov.b	r15, 6(r11)
.L21:
.LM30:
	mov.b	7(r11), r15
	cmp.b	r15, 6(r11)
	jne	.L18
.LM31:
	add.b	#llo(-1), &BusyPrio
.L15:
.LBE6:
.LM32:
	cmp.b	r10, &BusyPrio
	jne	.L19

	/* epilogue: frame size = 0 */
.LM33:
	pop	r10
.LVL12:
	pop	r11
.LVL13:
	ret
.LFE8:
.Lfe5:
	.size	HandleTasks,.Lfe5-HandleTasks
;; End of function